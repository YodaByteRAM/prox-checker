#!/usr/bin/python3
#https://stackoverflow.com/questions/4393086/https-proxy-tunneling-with-the-ssl-module
import os
import sys
import time
import threading
import socket
import copy
from OpenSSL import SSL

# Comments Pending
class Main(object):
    """
Created by Camerin Figueroa

Used to check if each proxy in
a list still works and if it is
an HTTP or HTTP/S Proxy

Usage: proxcheck.py [options]
    """
    def __init__(self): # Initializes the script.
        # Layout of each field:
        #    name:[Required(True/False),"Description",DefaultValue]
        self.default_fields = {# This is run when the Main class is initially started.
            'help':[False, 'Displays this message', None],
            'infile':[True, '''
Path to list of proxies.Requires each line with a proxy
to be formatted as a socket address with address and port
1.1.1.1:1234''', ''],
            'threads':[False,
                       '''Number of threads or workers to run when checking
proxies. Limited to 1000!!!''',
                       10],
            'outfile':[False,
                       'Path to the file which working proxies will be written to',
                       'workingproxies.txt'],
            'httpsite':[False,
                        '''The http website that the proxies will be used to
connect to when testing.''',
                        'checkip.dyndns.org/'],
            'httpssite':[False,
                         '''The https website that the proxies will be used
to connect to when testing.''',
                         'www.apple.com/']
            }
        # Arguments set by users are taken and set in the self.fields variable.
        self.fields = self.get_arguments()
        # This is the list where working scripts will be put.
        # When the script finishes this list will be written to the output file.
        self.http = []
        self.https = []
        self.initialtest()
    def initialtest(self):
        """
This initial test is run when the script first starts
to verify that the test sites are usable. If anything
other than a 200 OK is replied, the script prints and
error and dies.
        """
        hostname, path = self.urlinfo(self.fields['httpsite'][2])
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((hostname, 80))
        #print(('GET %s HTTP/1.1\r\n\r\n'%(path)))
        sock.send(('''GET %s HTTP/1.1\r\nAccept-Encoding: identity
Host: %s
Connection: close
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0
\r\n\r\n'''%(path, hostname)).encode())
        recv = sock.recv(1024)
        #print(recv)
        if "200 OK" not in  recv.decode().split("\r\n")[0]:
            print("Error [!]\nUnusable test website(HTTP)")
            sys.exit(0)
        sock.close()

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(10)
        sock.setblocking(1)
        hostname, path = self.urlinfo(self.fields['httpssite'][2])
        sock.connect((hostname, 443))
        ctx = SSL.Context(SSL.SSLv23_METHOD)
        ctx.set_verify(SSL.VERIFY_PEER, self.cbVerify)
        securesocket = SSL.Connection(ctx, sock)
        securesocket.set_connect_state()
        securesocket.do_handshake()
        securesocket.send("""GET %s HTTP/1.1
Accept-Encoding: identity
Host: %s
Connection: close
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0
\r\n\r\n"""%(path, hostname))
        if "200 OK" not in securesocket.recv(1024).decode().split("\r\n")[0]:
            print("Error [!]\nUnusable test website (HTTPS)")
            sys.exit(0)
        securesocket.shutdown()
        securesocket.close()
        sock.close()

    def get_arguments(self):
        """
Takes each argument and adds
their information to the copy
of self.default_fields to
fields dictionary. Once finished
the script returns the fields
dictionary.
        """
        fields = copy.copy(self.default_fields)
        #print(fields == self.default_fields)
        if len(sys.argv) <= 1:
            self.help("No Arguments")
        else:
            for i in range(1, len(sys.argv), 2):
                if sys.argv[i][2:] == "help":
                    self.help()
                    sys.exit()
                elif fields[sys.argv[i][2:]][2] == "threads":
                    if int(sys.argv[i+1]) > 1000:
                        fields[sys.argv[i][2:]][2] = 1000
                    else:
                        fields[sys.argv[i][2:]][2] = int(sys.argv[i+1])
                elif sys.argv[i][2:] in fields:
                    datatype = type(fields[sys.argv[i][2:]][2])
                    fields[sys.argv[i][2:]][2] = datatype(sys.argv[i+1])
                else:
                    self.help("Invalid argument %s" % (sys.argv[i]))
                    sys.exit()
        for i in fields:
            if fields[i][0] is True and fields[i][2] == self.default_fields[i][2]:
                self.help("Required field --%s missing" % i)
                sys.exit()
        return fields
    def help(self, reason=None):
        """
Prints the help menu. The Usage is printed, then
the script iterates over each
        """
        if reason != None:
            print(reason)
        print("""
Created by Camerin Figueroa
Used to check if each proxy in a list is working.
Usage: proxcheck.py [options]

Options:
        """)
        for o in self.default_fields:
            if self.default_fields[o][2] is None:
                print("    --%s:%s\n"%(o, self.default_fields[o][1]))
            else:
                print("    --%s [%s]:%s\n"%(o,
                                            self.default_fields[o][2],
                                            self.default_fields[o][1]))
        sys.exit()
    def urlinfo(self, url):
        """
Extracts the hostname/IP and
path which is in a url. This
url can be only the hostname/IP
or a full url.
Ex:
  In: "http://www.google.com/"
  Out: "www.google.com", "/"
        """
        if "/" not in url:
            return url, "/"
        else:
            if "://" in url:
                url = url.split("//")[1]
            url2 = url.split("/")
            if url[len(url2[0]):] == "/":
                return url2[0], "/"
            else:
                return url2[0], url[len(url2[0]):]

    def check(self, ip, port):
        if self.portcheck(ip, port) is True:
            #print("Port")
            if self.httpProxyCheck(ip, port) is True:
                #print("HTTP")
                if self.httpsProxyCheck(ip, port) is True:
                    #print("HTTPS")
                    self.https.append("%s:%s" % (ip, port))
                else:
                    self.http.append("%s:%s" % (ip, port))

    def portcheck(self, ip, port):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.settimeout(10)
        try:
            sock.connect((ip, port))
            sock.close()
            return True
        except:
            return False
    def cbVerify(self, conn, cert, errun, depth, ok):
        return True
    def httpsProxyCheck(self, ip, port):
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(10)
            sock.setblocking(1)
            sock.connect((ip,port))
            hostname,path = self.urlinfo(self.fields['httpssite'][2])
            #print(("CONNECT %s:443 HTTP/1.0\r\nConnection: close\r\n\r\n" % (hostname)))
            sock.send(("CONNECT %s:443 HTTP/1.0\r\nConnection: close\r\n\r\n" % (hostname)).encode())
            sock.recv(1024)
            ctx = SSL.Context(SSL.SSLv23_METHOD)
            ctx.set_verify(SSL.VERIFY_PEER, self.cbVerify)
            ss = SSL.Connection(ctx, sock)
            ss.set_connect_state()
            ss.do_handshake()
            ss.send("""GET %s HTTP/1.1
Accept-Encoding: identity
Host: %s
Connection: close
User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0\r\n\r\n"""%(path,hostname))
            recv = ss.recv(1024)
            #if recv not in ['',' '] and False not in [i not in recv for i in ['400 Bad Request','501 Tor','404 Not Found']]:
            print("200 OK" in recv.decode().split("\r\n")[0])
            if "200 OK" in recv.decode().split("\r\n")[0]:
                return True
            else:
                return False
            ss.shutdown()
            ss.close()
            sock.close()
        except:
            return False
    def httpProxyCheck(self,ip,port):
        try:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(10)
            sock.connect((ip,port))
            hostname,path = self.urlinfo(self.fields['httpsite'][2])
            sock.send(('GET http://%s HTTP/1.1\r\nUser-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:58.0) Gecko/20100101 Firefox/58.0\r\n\r\n'%(hostname+path)).encode())
            recv = sock.recv(1024)
            #print(recv,ip,port,recv.decode().split("\r\n"))
            #print(ip,port)
            #if recv.decode() not in ['',' '] and False not in [i not in recv.decode() for i in ['400 Bad Request','501 Tor','404 Not Found']]: # self.init_conn
            #print(recv.decode().split("\r\n"))
            #print(recv.decode().split("\r\n")[0])
            if "200 OK" in recv.decode().split("\r\n")[0]:
                return True
            else:
                return False
        except:
            return False
            try:
                sock.close()
            except:
                pass
    def generate_lines(self,document):
        lines = []
        f = open(self.fields['infile'][2],'r')
        for line in f:
            lines.append(line)
        f.close()
        return lines
    def main(self): # This is where the script reads the input file and starts threads to
        threads=[]
        lines = self.generate_lines(self.fields['infile'][2]) # The input file is read by the generate_lines function and a list is given to the lines variable
        lineNum = 0
        try:
            for p in lines:# Each proxy in the lines list is checked if it can be used and then given to a thread.
                print("\nTrying line %i out of %i: %s"%(lineNum,len(lines),p)) # A statement saying which proxy out of all proxies the last proxy is to be run and the socket of that proxy
                p = p.split("#")[0]
                p = p.split(";")[0]
                p = p.split('\n')[0]
                if p not in ['',' '] and ":" in p:
                    ip = p.split(":")[0]
                    try:
                        port = int(p.split(":")[1])
                    except:
                        port = p.split(":")[1]
                    if ip.count('.') > 0 and type(port) == int:
                        if len(threads) < self.fields['threads'][2]: # If there is an available spot in the threads list the current thread is added to the threads list
                            threads.append(threading.Thread(target=self.check,args=(ip,port)))
                            threads[len(threads)-1].start()
                        else:# Otherwise the script looks for an inactive thread in the threads list and replaces the first inactive thread it can find.
                            looped = 0
                            thread = None
                            while looped < self.fields['threads'][2] or thread == None:
                                if threads[looped].is_alive() == False:
                                    thread = looped
                                    looped = 100000000000000000000000
                                else:
                                    if looped >= self.fields['threads'][2]-1:
                                        looped = 0
                                    else:
                                        looped +=1
                            threads[thread] = threading.Thread(target=self.check,args=(ip,port))
                            threads[thread].start()
                lineNum += 1
            time.sleep(1)# Once all proxies finish running the script waits 1 second and then loops until all threads are inactive.
            online = True
            while online==True:
                online = False
                time.sleep(1)
                for t in threads:
                    if t.is_alive() == True:
                        online == True
        except KeyboardInterrupt: # If the script is killed with CTRL+C the script will prematurely finish.
            print("Script has been killed prematurely...")
        print("Saving working proxies to %s"%(self.fields['outfile'][2]))
        #self.http = list(set(self.http))
        f = open(self.fields['outfile'][2],'w') # The script finishes by writing the self.working list to the output file
        f.write("#HTTP\n%s\n#HTTPS\n%s"%(''.join(["%s\n"%(i)for i in self.http]),''.join(["%s\n"%(i)for i in self.https])))
        f.close()
        print("Working Proxies:\nHTTP:\n%s\nHTTP/S:\n%s" % (''.join(["    %s\n"%(i)for i in self.http]),''.join(["    %s\n"%(i)for i in self.https]))) # The list of working proxies is printed
        os._exit(0) # The script forces the process to kill

if __name__ == "__main__":
    main = Main()
    main.main()
