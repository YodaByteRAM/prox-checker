# Prox Checker

Prox Checker is a mulithreaded proxy checker that uses python3 and a few libraries to check large lists of proxies and outputs which proxies work. It is advised to use a vpn while testing these proxies.

#How to run:

- Clone the repository

`git clone https://YodaByteRAM@bitbucket.org/YodaByteRAM/prox-checker.git`

- Run the script with the argument --infile

`python3 proxchecker.py --infile proxylist.txt`

- You can specify up to 1000 threads by adding --threads #ofdesiredthreads

`python3 proxchecker.py --infile proxylist.txt --threads 300`

- By default the output file will be put in the working directory as workingproxies.txt. You can change this with the --outfile argument

`python3 proxchecker.py --infile proxylist.txt --threads 300 --outfile proxies2.0.txt`

#Features:
  - Ability to check multiple proxies at the same time.
  - Output to specific file
  - Input from specific file
  - Open Source
  - Up to 1000 Threads at a time!!!
  - Custom help menu
  - HTTPS proxy detection
  - Ability to choose test sites
#Changes:
  - proxchecker.py Created
  - Removed Scapy and httplib2 for maximum control over transmission of packets.

